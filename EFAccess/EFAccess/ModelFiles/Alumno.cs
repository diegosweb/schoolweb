namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Alumno")]
    public partial class Alumno
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Alumno()
        {
            AlumnoDocumento = new HashSet<AlumnoDocumento>();
            Calificacion = new HashSet<Calificacion>();
            InformacionMedica = new HashSet<InformacionMedica>();
            Inscripcion = new HashSet<Inscripcion>();
            TutorAlumno = new HashSet<TutorAlumno>();
            Usuario = new HashSet<Usuario>();
        }

        [Key]
        public int IdAlumno { get; set; }

        [Required]
        [StringLength(40)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(40)]
        public string Apellido { get; set; }

        [Required]
        [StringLength(1)]
        public string Sexo { get; set; }

        [Required]
        [StringLength(1)]
        public string Estado { get; set; }

        [Required]
        [StringLength(10)]
        public string DNI { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaNacimiento { get; set; }

        [StringLength(20)]
        public string Nacionalidad { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(15)]
        public string Celular { get; set; }

        [StringLength(15)]
        public string Telefono1 { get; set; }

        [StringLength(15)]
        public string Telefono2 { get; set; }

        [StringLength(40)]
        public string DireccionCalle { get; set; }

        [StringLength(20)]
        public string DireccionNumero { get; set; }

        [StringLength(40)]
        public string DireccionBarrio { get; set; }

        [StringLength(30)]
        public string DireccionCiudad { get; set; }

        [StringLength(30)]
        public string DireccionProvincia { get; set; }

        //[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AlumnoDocumento> AlumnoDocumento { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Calificacion> Calificacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<InformacionMedica> InformacionMedica { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Inscripcion> Inscripcion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TutorAlumno> TutorAlumno { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Usuario> Usuario { get; set; }
    }
}
