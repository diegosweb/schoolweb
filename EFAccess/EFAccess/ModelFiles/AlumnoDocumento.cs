namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AlumnoDocumento")]
    public partial class AlumnoDocumento
    {
        [Key]
        public int IdAlumnoDocumento { get; set; }

        public int IdAlumno { get; set; }

        public int IdDocumento { get; set; }

        public DateTime Fecha { get; set; }

        [Required]
        [StringLength(1)]
        public string Presentado { get; set; }

        [Column(TypeName = "text")]
        public string Comentario { get; set; }

        public virtual Alumno Alumno { get; set; }

        public virtual Documento Documento { get; set; }
    }
}
