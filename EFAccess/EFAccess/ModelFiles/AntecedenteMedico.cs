namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AntecedenteMedico")]
    public partial class AntecedenteMedico
    {
        [Key]
        public int IdAntecedenteMedico { get; set; }

        public int IdAntecendenteMedico { get; set; }

        public DateTime Fecha { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Descripcion { get; set; }

        public virtual InformacionMedica InformacionMedica { get; set; }
    }
}
