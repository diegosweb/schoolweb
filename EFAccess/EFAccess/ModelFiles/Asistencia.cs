namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Asistencia")]
    public partial class Asistencia
    {
        [Key]
        public int IdAsistencia { get; set; }

        public int IdInscripcion { get; set; }

        public int IdListaAsistencia { get; set; }

        [Required]
        [StringLength(1)]
        public string Estado { get; set; }

        [StringLength(1)]
        public string Justificacion { get; set; }

        [StringLength(40)]
        public string Observaciones { get; set; }

        public virtual Inscripcion Inscripcion { get; set; }

        public virtual ListaAsistencia ListaAsistencia { get; set; }
    }
}
