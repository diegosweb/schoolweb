namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Aula")]
    public partial class Aula
    {
        [Key]
        public int IdAula { get; set; }

        [Required]
        [StringLength(20)]
        public string Descripcion { get; set; }

        [StringLength(40)]
        public string Localizacion { get; set; }
    }
}
