namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Calificacion")]
    public partial class Calificacion
    {
        [Key]
        public int IdCalificacion { get; set; }

        public int IdAlumno { get; set; }

        public int IdExamen { get; set; }

        public decimal Nota { get; set; }

        public virtual Alumno Alumno { get; set; }

        public virtual Examen Examen { get; set; }
    }
}
