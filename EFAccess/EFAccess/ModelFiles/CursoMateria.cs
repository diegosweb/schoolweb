namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CursoMateria")]
    public partial class CursoMateria
    {
        [Key]
        public int IdCursoMateria { get; set; }

        public int IdCurso { get; set; }

        public int IdMateria { get; set; }

        public virtual Curso Curso { get; set; }

        public virtual Materia Materia { get; set; }
    }
}
