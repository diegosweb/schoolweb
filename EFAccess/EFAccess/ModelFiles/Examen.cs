namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Examen
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Examen()
        {
            Calificacion = new HashSet<Calificacion>();
        }

        [Key]
        public int IdExamen { get; set; }

        [Required]
        [StringLength(40)]
        public string Nombre { get; set; }

        public int IdTipoCalificacion { get; set; }

        public int IdCurso { get; set; }

        public int IdCicloLectivo { get; set; }

        public int IdMateria { get; set; }

        public DateTime FechaExamen { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Calificacion> Calificacion { get; set; }

        public virtual CicloLectivo CicloLectivo { get; set; }

        public virtual Curso Curso { get; set; }

        public virtual Materia Materia { get; set; }

        public virtual TipoCalificacion TipoCalificacion { get; set; }
    }
}
