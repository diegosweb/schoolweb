namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("InformacionMedica")]
    public partial class InformacionMedica
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public InformacionMedica()
        {
            AntecedenteMedico = new HashSet<AntecedenteMedico>();
        }

        [Key]
        public int IdAntecendenteMedico { get; set; }

        public int IdAlumno { get; set; }

        [StringLength(60)]
        public string NombrePediatra { get; set; }

        [StringLength(15)]
        public string TelefonoPediatra { get; set; }

        public virtual Alumno Alumno { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AntecedenteMedico> AntecedenteMedico { get; set; }
    }
}
