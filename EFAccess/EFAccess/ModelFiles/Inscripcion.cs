namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Inscripcion")]
    public partial class Inscripcion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Inscripcion()
        {
            Asistencia = new HashSet<Asistencia>();
        }

        [Key]
        public int IdInscripcion { get; set; }

        public int IdAlumno { get; set; }

        public int IdCicloLectivo { get; set; }

        public int IdCurso { get; set; }

        public DateTime Fecha { get; set; }

        public virtual Alumno Alumno { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Asistencia> Asistencia { get; set; }

        public virtual CicloLectivo CicloLectivo { get; set; }

        public virtual Curso Curso { get; set; }
    }
}
