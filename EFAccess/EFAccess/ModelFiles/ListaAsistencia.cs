namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ListaAsistencia")]
    public partial class ListaAsistencia
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ListaAsistencia()
        {
            Asistencia = new HashSet<Asistencia>();
        }

        [Key]
        public int IdListaAsistencia { get; set; }

        public int IdCicloLectivo { get; set; }

        public int IdCurso { get; set; }

        [Column(TypeName = "date")]
        public DateTime Fecha { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Asistencia> Asistencia { get; set; }

        public virtual CicloLectivo CicloLectivo { get; set; }

        public virtual Curso Curso { get; set; }
    }
}
