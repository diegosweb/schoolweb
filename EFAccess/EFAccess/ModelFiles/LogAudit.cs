namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("LogAudit")]
    public partial class LogAudit
    {
        [Key]
        public int idLogAudit { get; set; }

        public int? idUsuario { get; set; }

        [StringLength(20)]
        public string Objeto { get; set; }

        [Required]
        [StringLength(20)]
        public string CodOperacion { get; set; }

        public DateTime Fecha { get; set; }

        [StringLength(255)]
        public string ValorViejo { get; set; }

        [StringLength(255)]
        public string ValorNuevo { get; set; }
    }
}
