namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Materia")]
    public partial class Materia
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Materia()
        {
            CursoMateria = new HashSet<CursoMateria>();
            Examen = new HashSet<Examen>();
            ProfesorMateria = new HashSet<ProfesorMateria>();
        }

        [Key]
        public int IdMateria { get; set; }

        public int IdCicloLectivo { get; set; }

        [Required]
        [StringLength(40)]
        public string Descripcion { get; set; }

        [StringLength(40)]
        public string DescripcionSimple { get; set; }

        public virtual CicloLectivo CicloLectivo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CursoMateria> CursoMateria { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Examen> Examen { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProfesorMateria> ProfesorMateria { get; set; }
    }
}
