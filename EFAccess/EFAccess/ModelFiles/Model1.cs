namespace EFAccess
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class EFAccessModel : DbContext
    {
        public EFAccessModel()
            : base("name=SchoolConnectionString")
        {
        }

        public virtual DbSet<Alumno> Alumno { get; set; }
        public virtual DbSet<AlumnoDocumento> AlumnoDocumento { get; set; }
        public virtual DbSet<AntecedenteMedico> AntecedenteMedico { get; set; }
        public virtual DbSet<Asistencia> Asistencia { get; set; }
        public virtual DbSet<Aula> Aula { get; set; }
        public virtual DbSet<Calificacion> Calificacion { get; set; }
        public virtual DbSet<CicloLectivo> CicloLectivo { get; set; }
        public virtual DbSet<Curso> Curso { get; set; }
        public virtual DbSet<CursoMateria> CursoMateria { get; set; }
        public virtual DbSet<Documento> Documento { get; set; }
        public virtual DbSet<Examen> Examen { get; set; }
        public virtual DbSet<InformacionMedica> InformacionMedica { get; set; }
        public virtual DbSet<Inscripcion> Inscripcion { get; set; }
        public virtual DbSet<ListaAsistencia> ListaAsistencia { get; set; }
        public virtual DbSet<LogAudit> LogAudit { get; set; }
        public virtual DbSet<Materia> Materia { get; set; }
        public virtual DbSet<Nivel> Nivel { get; set; }
        public virtual DbSet<PeriodoLectivo> PeriodoLectivo { get; set; }
        public virtual DbSet<Preceptor> Preceptor { get; set; }
        public virtual DbSet<Preferencia> Preferencia { get; set; }
        public virtual DbSet<Profesor> Profesor { get; set; }
        public virtual DbSet<ProfesorCurso> ProfesorCurso { get; set; }
        public virtual DbSet<ProfesorMateria> ProfesorMateria { get; set; }
        public virtual DbSet<ReglaAsistencia> ReglaAsistencia { get; set; }
        public virtual DbSet<Relacion> Relacion { get; set; }
        public virtual DbSet<Rol> Rol { get; set; }
        public virtual DbSet<TipoCalificacion> TipoCalificacion { get; set; }
        public virtual DbSet<Transaccion> Transaccion { get; set; }
        public virtual DbSet<TransaccionesRoles> TransaccionesRoles { get; set; }
        public virtual DbSet<Tutor> Tutor { get; set; }
        public virtual DbSet<TutorAlumno> TutorAlumno { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }
        public virtual DbSet<UsuariosRoles> UsuariosRoles { get; set; }
        public virtual DbSet<zBkpCurso> zBkpCurso { get; set; }
        public virtual DbSet<vwBoletin> vwBoletin { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Alumno>()
                .HasMany(e => e.AlumnoDocumento)
                .WithRequired(e => e.Alumno)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Alumno>()
                .HasMany(e => e.Calificacion)
                .WithRequired(e => e.Alumno)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Alumno>()
                .HasMany(e => e.InformacionMedica)
                .WithRequired(e => e.Alumno)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Alumno>()
                .HasMany(e => e.Inscripcion)
                .WithRequired(e => e.Alumno)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Alumno>()
                .HasMany(e => e.TutorAlumno)
                .WithRequired(e => e.Alumno)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AlumnoDocumento>()
                .Property(e => e.Comentario)
                .IsUnicode(false);

            modelBuilder.Entity<AntecedenteMedico>()
                .Property(e => e.Descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<Calificacion>()
                .Property(e => e.Nota)
                .HasPrecision(10, 2);

            modelBuilder.Entity<CicloLectivo>()
                .HasMany(e => e.Examen)
                .WithRequired(e => e.CicloLectivo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CicloLectivo>()
                .HasMany(e => e.Inscripcion)
                .WithRequired(e => e.CicloLectivo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CicloLectivo>()
                .HasMany(e => e.ListaAsistencia)
                .WithRequired(e => e.CicloLectivo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CicloLectivo>()
                .HasMany(e => e.Materia)
                .WithRequired(e => e.CicloLectivo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CicloLectivo>()
                .HasMany(e => e.PeriodoLectivo)
                .WithRequired(e => e.CicloLectivo)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Curso>()
                .HasMany(e => e.CursoMateria)
                .WithRequired(e => e.Curso)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Curso>()
                .HasMany(e => e.Examen)
                .WithRequired(e => e.Curso)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Curso>()
                .HasMany(e => e.Inscripcion)
                .WithRequired(e => e.Curso)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Curso>()
                .HasMany(e => e.ListaAsistencia)
                .WithRequired(e => e.Curso)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Curso>()
                .HasMany(e => e.ProfesorCurso)
                .WithRequired(e => e.Curso)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Documento>()
                .HasMany(e => e.AlumnoDocumento)
                .WithRequired(e => e.Documento)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Examen>()
                .HasMany(e => e.Calificacion)
                .WithRequired(e => e.Examen)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<InformacionMedica>()
                .HasMany(e => e.AntecedenteMedico)
                .WithRequired(e => e.InformacionMedica)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Inscripcion>()
                .HasMany(e => e.Asistencia)
                .WithRequired(e => e.Inscripcion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ListaAsistencia>()
                .HasMany(e => e.Asistencia)
                .WithRequired(e => e.ListaAsistencia)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Materia>()
                .HasMany(e => e.CursoMateria)
                .WithRequired(e => e.Materia)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Materia>()
                .HasMany(e => e.Examen)
                .WithRequired(e => e.Materia)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Materia>()
                .HasMany(e => e.ProfesorMateria)
                .WithRequired(e => e.Materia)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Nivel>()
                .HasMany(e => e.Curso)
                .WithRequired(e => e.Nivel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Nivel>()
                .HasMany(e => e.Profesor)
                .WithRequired(e => e.Nivel)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Profesor>()
                .HasMany(e => e.ProfesorCurso)
                .WithRequired(e => e.Profesor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Profesor>()
                .HasMany(e => e.ProfesorMateria)
                .WithRequired(e => e.Profesor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ReglaAsistencia>()
                .Property(e => e.Ausencia1)
                .HasPrecision(7, 2);

            modelBuilder.Entity<ReglaAsistencia>()
                .Property(e => e.Ausencia2)
                .HasPrecision(7, 2);

            modelBuilder.Entity<ReglaAsistencia>()
                .Property(e => e.Ausencia3)
                .HasPrecision(7, 2);

            modelBuilder.Entity<ReglaAsistencia>()
                .Property(e => e.AusenciaTotal)
                .HasPrecision(7, 2);

            modelBuilder.Entity<Relacion>()
                .HasMany(e => e.TutorAlumno)
                .WithRequired(e => e.Relacion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Rol>()
                .HasMany(e => e.TransaccionesRoles)
                .WithRequired(e => e.Rol)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Rol>()
                .HasMany(e => e.UsuariosRoles)
                .WithRequired(e => e.Rol)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TipoCalificacion>()
                .HasMany(e => e.Examen)
                .WithRequired(e => e.TipoCalificacion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Transaccion>()
                .HasMany(e => e.TransaccionesRoles)
                .WithRequired(e => e.Transaccion)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Tutor>()
                .HasMany(e => e.TutorAlumno)
                .WithRequired(e => e.Tutor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Usuario>()
                .HasMany(e => e.UsuariosRoles)
                .WithRequired(e => e.Usuario)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<vwBoletin>()
                .Property(e => e.N1_1)
                .HasPrecision(10, 2);

            modelBuilder.Entity<vwBoletin>()
                .Property(e => e.N1_2)
                .HasPrecision(10, 2);

            modelBuilder.Entity<vwBoletin>()
                .Property(e => e.N1_3)
                .HasPrecision(10, 2);

            modelBuilder.Entity<vwBoletin>()
                .Property(e => e.N1_4)
                .HasPrecision(10, 2);

            modelBuilder.Entity<vwBoletin>()
                .Property(e => e.N1_5)
                .HasPrecision(10, 2);

            modelBuilder.Entity<vwBoletin>()
                .Property(e => e.N2_1)
                .HasPrecision(10, 2);

            modelBuilder.Entity<vwBoletin>()
                .Property(e => e.N2_2)
                .HasPrecision(10, 2);

            modelBuilder.Entity<vwBoletin>()
                .Property(e => e.N2_3)
                .HasPrecision(10, 2);

            modelBuilder.Entity<vwBoletin>()
                .Property(e => e.N2_4)
                .HasPrecision(10, 2);

            modelBuilder.Entity<vwBoletin>()
                .Property(e => e.N2_5)
                .HasPrecision(10, 2);

            modelBuilder.Entity<vwBoletin>()
                .Property(e => e.N3_1)
                .HasPrecision(10, 2);

            modelBuilder.Entity<vwBoletin>()
                .Property(e => e.N3_2)
                .HasPrecision(10, 2);

            modelBuilder.Entity<vwBoletin>()
                .Property(e => e.N3_3)
                .HasPrecision(10, 2);

            modelBuilder.Entity<vwBoletin>()
                .Property(e => e.N3_4)
                .HasPrecision(10, 2);

            modelBuilder.Entity<vwBoletin>()
                .Property(e => e.N3_5)
                .HasPrecision(10, 2);
        }
    }
}
