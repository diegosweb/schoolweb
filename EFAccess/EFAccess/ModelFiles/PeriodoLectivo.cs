namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PeriodoLectivo")]
    public partial class PeriodoLectivo
    {
        [Key]
        public int IdPeriodoLectivo { get; set; }

        [StringLength(20)]
        public string Nombre { get; set; }

        public int Numero { get; set; }

        public int IdCicloLectivo { get; set; }

        public DateTime FechaInicio { get; set; }

        public DateTime FechaFin { get; set; }

        public virtual CicloLectivo CicloLectivo { get; set; }
    }
}
