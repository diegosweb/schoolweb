namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Preferencia")]
    public partial class Preferencia
    {
        [Key]
        public int IdPreferencia { get; set; }

        [Required]
        [StringLength(20)]
        public string Codigo { get; set; }

        [StringLength(40)]
        public string Valor { get; set; }
    }
}
