namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Profesor")]
    public partial class Profesor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Profesor()
        {
            ProfesorCurso = new HashSet<ProfesorCurso>();
            ProfesorMateria = new HashSet<ProfesorMateria>();
            Usuario = new HashSet<Usuario>();
        }

        [Key]
        public int IdProfesor { get; set; }

        [Required]
        [StringLength(40)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(40)]
        public string Apellido { get; set; }

        public int IdNivel { get; set; }

        [StringLength(10)]
        public string DNI { get; set; }

        public DateTime? FechaNacimiento { get; set; }

        [StringLength(20)]
        public string Nacionalidad { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(15)]
        public string Celular { get; set; }

        [StringLength(15)]
        public string Telefono1 { get; set; }

        [StringLength(15)]
        public string Telefono2 { get; set; }

        [StringLength(40)]
        public string DireccionCalle { get; set; }

        [StringLength(20)]
        public string DireccionNumero { get; set; }

        [StringLength(40)]
        public string DireccionBarrio { get; set; }

        [StringLength(30)]
        public string DireccionCiudad { get; set; }

        [StringLength(30)]
        public string DireccionProvincia { get; set; }

        public DateTime FechaCreacion { get; set; }

        public DateTime? FechaModificacion { get; set; }

        public virtual Nivel Nivel { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProfesorCurso> ProfesorCurso { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProfesorMateria> ProfesorMateria { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Usuario> Usuario { get; set; }
    }
}
