namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProfesorCurso")]
    public partial class ProfesorCurso
    {
        [Key]
        public int IdProfesorCurso { get; set; }

        public int IdProfesor { get; set; }

        public int IdCurso { get; set; }

        public virtual Curso Curso { get; set; }

        public virtual Profesor Profesor { get; set; }
    }
}
