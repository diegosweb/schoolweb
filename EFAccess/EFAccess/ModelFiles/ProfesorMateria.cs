namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ProfesorMateria")]
    public partial class ProfesorMateria
    {
        [Key]
        public int IdProfesorMateria { get; set; }

        public int IdProfesor { get; set; }

        public int IdMateria { get; set; }

        [Required]
        [StringLength(1)]
        public string Categoria { get; set; }

        public virtual Materia Materia { get; set; }

        public virtual Profesor Profesor { get; set; }
    }
}
