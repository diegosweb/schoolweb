namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ReglaAsistencia")]
    public partial class ReglaAsistencia
    {
        [Key]
        public int IdReglaAsistencia { get; set; }

        [StringLength(1)]
        public string Categoria1 { get; set; }

        [StringLength(1)]
        public string Categoria2 { get; set; }

        [StringLength(1)]
        public string Categoria3 { get; set; }

        [StringLength(1)]
        public string Asistencia1 { get; set; }

        [StringLength(1)]
        public string Asistencia2 { get; set; }

        [StringLength(1)]
        public string Asistencia3 { get; set; }

        public decimal? Ausencia1 { get; set; }

        public decimal? Ausencia2 { get; set; }

        public decimal? Ausencia3 { get; set; }

        public decimal? AusenciaTotal { get; set; }
    }
}
