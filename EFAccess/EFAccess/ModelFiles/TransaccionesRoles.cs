namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class TransaccionesRoles
    {
        [Key]
        public int IdTransaccionesRoles { get; set; }

        public int IdTransaccion { get; set; }

        public int IdRol { get; set; }

        public virtual Rol Rol { get; set; }

        public virtual Transaccion Transaccion { get; set; }
    }
}
