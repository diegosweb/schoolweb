namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Tutor")]
    public partial class Tutor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Tutor()
        {
            TutorAlumno = new HashSet<TutorAlumno>();
        }

        [Key]
        public int IdTutor { get; set; }

        [Required]
        [StringLength(40)]
        public string Nombre { get; set; }

        [Required]
        [StringLength(40)]
        public string Apellido { get; set; }

        [StringLength(10)]
        public string DNI { get; set; }

        [Required]
        [StringLength(1)]
        public string AutorizadoRetirar { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(15)]
        public string Celular { get; set; }

        [StringLength(15)]
        public string Telefono1 { get; set; }

        [StringLength(15)]
        public string Telefono2 { get; set; }

        [StringLength(30)]
        public string DireccionCalle { get; set; }

        [StringLength(20)]
        public string DireccionNumero { get; set; }

        [StringLength(40)]
        public string DireccionBarrio { get; set; }

        [StringLength(30)]
        public string DireccionCiudad { get; set; }

        [StringLength(30)]
        public string DireccionProvincia { get; set; }

        [StringLength(30)]
        public string DireccionLaboralCalle { get; set; }

        [StringLength(20)]
        public string DireccionLaboralNumero { get; set; }

        [StringLength(40)]
        public string DireccionLaboralBarrio { get; set; }

        [StringLength(30)]
        public string DireccionLaboralCiudad { get; set; }

        [StringLength(30)]
        public string DireccionLaboralProvincia { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TutorAlumno> TutorAlumno { get; set; }
    }
}
