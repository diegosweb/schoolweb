namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TutorAlumno")]
    public partial class TutorAlumno
    {
        [Key]
        public int IdTutorAlumno { get; set; }

        public int IdTutor { get; set; }

        public int IdAlumno { get; set; }

        public int IdRelacion { get; set; }

        public virtual Alumno Alumno { get; set; }

        public virtual Relacion Relacion { get; set; }

        public virtual Tutor Tutor { get; set; }
    }
}
