namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Usuario")]
    public partial class Usuario
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Usuario()
        {
            UsuariosRoles = new HashSet<UsuariosRoles>();
        }

        [Key]
        public int IdUsuario { get; set; }

        [Required]
        [StringLength(20)]
        public string Login { get; set; }

        public int? IdAlumno { get; set; }

        public int? IdProfesor { get; set; }

        public int? IdPreceptor { get; set; }

        [StringLength(60)]
        public string Nombre { get; set; }

        [StringLength(40)]
        public string Clave { get; set; }

        [Required]
        [StringLength(1)]
        public string Super { get; set; }

        [Required]
        [StringLength(1)]
        public string Habilitado { get; set; }

        public int ContadorFallos { get; set; }

        [Required]
        [StringLength(1)]
        public string Bloqueado { get; set; }

        public DateTime FechaCreacion { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        public virtual Alumno Alumno { get; set; }

        public virtual Preceptor Preceptor { get; set; }

        public virtual Profesor Profesor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UsuariosRoles> UsuariosRoles { get; set; }
    }
}
