namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UsuariosRoles
    {
        [Key]
        public int IdUsuariosRoles { get; set; }

        public int IdRol { get; set; }

        public int IdUsuario { get; set; }

        public virtual Rol Rol { get; set; }

        public virtual Usuario Usuario { get; set; }
    }
}
