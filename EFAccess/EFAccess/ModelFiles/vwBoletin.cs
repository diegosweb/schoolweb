namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("vwBoletin")]
    public partial class vwBoletin
    {
        [Key]
        public int IdExamen { get; set; }

        [StringLength(40)]
        public string Nombre { get; set; }

        [Column("N1.1")]
        public decimal? N1_1 { get; set; }

        [Column("N1.2")]
        public decimal? N1_2 { get; set; }

        [Column("N1.3")]
        public decimal? N1_3 { get; set; }

        [Column("N1.4")]
        public decimal? N1_4 { get; set; }

        [Column("N1.5")]
        public decimal? N1_5 { get; set; }

        [Column("N2.1")]
        public decimal? N2_1 { get; set; }

        [Column("N2.2")]
        public decimal? N2_2 { get; set; }

        [Column("N2.3")]
        public decimal? N2_3 { get; set; }

        [Column("N2.4")]
        public decimal? N2_4 { get; set; }

        [Column("N2.5")]
        public decimal? N2_5 { get; set; }

        [Column("N3.1")]
        public decimal? N3_1 { get; set; }

        [Column("N3.2")]
        public decimal? N3_2 { get; set; }

        [Column("N3.3")]
        public decimal? N3_3 { get; set; }

        [Column("N3.4")]
        public decimal? N3_4 { get; set; }

        [Column("N3.5")]
        public decimal? N3_5 { get; set; }
    }
}
