namespace EFAccess
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("zBkpCurso")]
    public partial class zBkpCurso
    {
        [Key]
        [Column(Order = 0)]
        public int IdCurso { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int IdNivel { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(1)]
        public string Categoria { get; set; }

        [Key]
        [Column(Order = 3)]
        [StringLength(15)]
        public string Division { get; set; }

        [StringLength(1)]
        public string Turno { get; set; }

        public int? Vacantes { get; set; }
    }
}
